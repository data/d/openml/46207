# OpenML dataset: Wind-Power

https://www.openml.org/d/46207

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Wind power production in MW recorded per every 4 seconds starting from 01/08/2019 in Australia.

From the website:
-----
This dataset contains a single very long daily time series representing the wind power production in MW recorded per every 4 seconds starting from 01/08/2019. It was downloaded from the Australian Energy Market Operator (AEMO) online platform. The length of this time series is 7397147.
-----

Here is the dataset curated by the Monash Time Series Forecasting Repository. It is not clear which were the preprocessing steps and how did they
acquired the data from the original website (https://aemo.com.au/ and http://www.nemweb.com.au/).

There are 3 columns:

id_series: The identifier of a time series.

value_0: The value of the time series at 'time_step'.

time_step: The time step on the time series.

date: The date of the time series in the format %Y-%m-%d.

Preprocessing:

1 - Renamed columns 'series_name' and 'series_value' to 'id_series' and 'value_0'.

2 - Exploded the 'value' column.

3 - Created 'time_step' column from the exploded data.

4 - Cretead 'date' column from the 'start_timestamp' and 'time_step' column, by offseting the 'start_timestamp' by 4 secods * time_step.

5 - Dropped 'start_timestamp' column. Defined 'id_series' as 'category' and casted 'value_0' to float.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46207) of an [OpenML dataset](https://www.openml.org/d/46207). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46207/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46207/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46207/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

